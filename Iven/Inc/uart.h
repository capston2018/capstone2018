#include <stdint.h>

void uart_send_message(uint8_t *v, uint8_t length);
void uart_receive_message(uint8_t *r, uint8_t length);
